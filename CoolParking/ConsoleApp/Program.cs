﻿using CoolParking.BL.Services;
using CoolParking.BL.UI;

namespace ConsoleApp
{
    class Program
    {
       
        static void Main(string[] args)
        {
            ConsoleUi console =new ConsoleUi(new ParkingService());
            console.Start();
        }
    }
}
