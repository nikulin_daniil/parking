﻿using System;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.UI
{
   public  class ConsoleUi
    {
        private IParkingService ps;

        public ConsoleUi(IParkingService ps)
        {
            this.ps = ps;
        }

        public void Start()
        {
            bool key = true;
            do
            {
                try
                {
                    int answer = 0;
                    Console.WriteLine("Please, choose option:\n" +
                                      "1:Show Parking Balance\n" +
                                      "2:Show capacity of parking\n" +
                                      "3:Amount of free slots\n" +
                                      "4:Last parking transaction\n" +
                                      "5:Show log of transaction\n" +
                                      "6:Show vehicles in parking\n" +
                                      "7:Add vehicles to parking\n" +
                                      "8:Remove vehicles from parking\n" +
                                      "9:Top up vehicle\n" +
                                      "0:Exit");

                    answer = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    switch (answer)
                    {
                        case 1:
                            ShowParkingBalance();
                            break;
                        case 2:
                            ShowBalaced();
                            break;
                        case 3:
                            ShowAmountOfFreePlaces();
                            break;
                        case 4:
                            GetLastParkingTransactions();
                            break;
                        case 5:
                            ShowLog()
                                ;
                            break;
                        case 6:
                            ShowVehicles();
                            break;
                        case 7:
                            AddVehicle();
                            break;
                        case 8:
                            RemoveVehicle();
                            break;
                        case 9:
                            TopUpVehicle();
                            break;
                        case 0:
                            key = false;
                            break;
                        default:
                            Console.WriteLine("Invalid command");
                            break;

                    }

                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine("Invalid input");
                }
                finally
                {
                    Console.WriteLine("Press key to continue");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
            while (key);
        }
        void ShowParkingBalance()
        {
            Console.WriteLine("Balance of parking:{0}",ps.GetBalance());
        }

        void ShowBalaced()
        {
            Console.WriteLine("Capacity of parking:{0}",ps.GetCapacity());
        }

        void ShowAmountOfFreePlaces()
        {
            Console.WriteLine("Amount of free places:{0}",ps.GetFreePlaces());
        }

        void GetLastParkingTransactions()
        {
            TransactionInfo[] transactionTemp = ps.GetLastParkingTransactions();
            if (transactionTemp.Length == 0) Console.WriteLine("Transaction Log is unwritten");
            foreach (var t in transactionTemp)
            {
                Console.WriteLine(t);
            }
        }

        void ShowLog()
        {
            try
            {
                Console.WriteLine("Transaction Log:\n {0}", ps.ReadFromLog());
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Log doesn`t exits");
            }
        }

        void ShowVehicles()
        {
            ReadOnlyCollection<Vehicle> tempVehicles=ps.GetVehicles();
            if (tempVehicles.Count == 0)
            {
                Console.WriteLine("Parking is empty");
                return;
            }
            foreach (var v in tempVehicles)
            {
                Console.WriteLine(v);
            }
        }

        void AddVehicle()
        {
            bool key = true;
            do
            {
                try
                {
                    Console.WriteLine("Enter ID:");
                    string id = Console.ReadLine();
                    Console.WriteLine("Enter Balance:");
                    int balance = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    VehicleType vehicleType = ChooseVehicleType();
                    Console.WriteLine("Is it valid info:{0}", new Vehicle(id, vehicleType, balance));
                    string answer = Console.ReadLine();
                    switch (answer.ToLower())
                    {
                        case "yes":
                        case "y":
                            key = false;
                            ps.AddVehicle(new Vehicle(id, vehicleType, balance));
                            break;
                        case "no":
                        case "n":
                            Console.WriteLine("Reenter");
                            break;
                        default:
                            Console.WriteLine("Invalid input\nReenter");
                            break;
                    }
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine("Invalid input");
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Invalid input");
                }
            } while (key);
        }

        void RemoveVehicle()
        {
            try
            {
                Console.WriteLine("Enter ID");
                ps.RemoveVehicle(Console.ReadLine());
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Invalid input");
            }
        }
        void TopUpVehicle()
        {
            try
            {

                Console.WriteLine("Enter ID");
                string id = Console.ReadLine();
                Console.WriteLine("Enter Sum");
                decimal sum = decimal.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                ps.TopUpVehicle(id, sum);
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Invalid input");
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Invalid input");
            }
        }
        VehicleType ChooseVehicleType()
        {
            bool key = true;
            do
            {
                Console.WriteLine("Choose vehicle type:\n" +
                                  "1:PassengerCar\n" +
                                  "2:Truck\n" +
                                  "3:Bus\n" +
                                  "4:Motorcycle\n");
                int vehicleType = int.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                switch (vehicleType)
                {
                    case 1:
                        return VehicleType.PassengerCar;
                    case 2:
                        return VehicleType.Truck;
                    case 3: 
                        return VehicleType.Bus;
                    case 4:
                        return VehicleType.Motorcycle;
                    default:
                        Console.WriteLine("Invalid input");
                        break;

                }
            } while (key);

            return VehicleType.none;
        }
    }
}
