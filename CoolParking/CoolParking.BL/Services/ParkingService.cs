﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.VisualBasic;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ILogService logService;
        private readonly ITimerService timerLog;
        public List<TransactionInfo> transaction;

        public ParkingService()
        {
            timerLog = new TimerService();
            _withdrawTimer = new TimerService();
            logService = new LogService();
            transaction = new List<TransactionInfo>();
            Parking.GetInstance();
            TimersInit();
        }
        public ParkingService(ITimerService _withdrawTimer, ITimerService timerLog, ILogService logService)
        {
            this.logService = logService;
            this.timerLog = timerLog;
            this._withdrawTimer = _withdrawTimer;
            transaction = new List<TransactionInfo>();
            Parking.GetInstance();
            TimersInit();
        }

        private void _withdrawTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            decimal result = 0m;
            foreach (var v in Parking.instance._vehicle)
            {
                if (v.Balance >= Settings.Tariffs[v.VehicleType])
                {
                    result = Settings.Tariffs[v.VehicleType];
                    v.Balance -= result;
                }
                else if (v.Balance > 0 && v.Balance < Settings.Tariffs[v.VehicleType])
                {
                    result = v.Balance + Math.Abs(v.Balance - Settings.Tariffs[v.VehicleType]) * Settings.CoefForFine;
                    v.Balance -= result;
                }
                else
                {
                    result = Settings.Tariffs[v.VehicleType] * Settings.CoefForFine;
                    v.Balance -= result;
                }
                transaction.Add(new TransactionInfo(v.Id,result,e.SignalTime));
            }
        }

        void TimersInit()
        {
            _withdrawTimer.Elapsed += _withdrawTimer_Elapsed;
            timerLog.Elapsed += TimerLog_Elapsed;
            _withdrawTimer.Interval = Settings.PeriodOfPayment;
            timerLog.Interval = Settings.PeriodOfPaymentLog;
            timerLog.Start();
            _withdrawTimer.Start();
        }

        private void TimerLog_Elapsed(object sender, ElapsedEventArgs e)
        {
            foreach (var t in transaction)
                logService.Write(t.ToString());
            Parking.instance.Balance += GetTransactionBalance();
            transaction.Clear();

            }

        public void Dispose()
        {
            _withdrawTimer.Dispose();
            timerLog.Dispose();
            Parking.instance._vehicle.Clear();
            Parking.instance.Balance = 0;
        }

        public decimal GetBalance()
        {
            return Parking.instance.Balance;
        }

        public decimal GetTransactionBalance()
        {
            if (transaction.Count != 0)
            {
                return transaction.Sum(x => x.Sum);

            }
            return 0;
        }
        public int GetCapacity()
        {
            return Settings.CapacityOfParking;
        }

        public int GetFreePlaces()
        {
            return Parking.instance.AmountOfFreeSlots();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var result = new ReadOnlyCollection<Vehicle>(Parking.instance._vehicle);
            return result;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (Parking.instance.Contains(vehicle.Id)) throw new ArgumentException();
            Parking.instance.AddVehicle(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!Parking.instance.Contains(vehicleId)) throw new ArgumentException();
            Parking.instance.RemoveVehicle(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0 || Parking.instance.Find(vehicleId) is null) throw new ArgumentException();
            Parking.instance.Find(vehicleId).Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transaction.ToArray();
        }
        public string ReadFromLog()
        {
            return logService.Read();
        }

    }
}