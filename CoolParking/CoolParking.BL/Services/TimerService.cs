﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService:ITimerService
    {
        readonly Timer  _timer =new Timer();
        public event ElapsedEventHandler Elapsed;
        public double Interval {
            get => _timer.Interval;
            set => _timer.Interval=value;
        }
        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this,null);
        }
        public void Start()
        {
            _timer.Elapsed += Elapsed;
            _timer.AutoReset = true;
            _timer.Enabled = true;
            _timer.Start();
          
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}