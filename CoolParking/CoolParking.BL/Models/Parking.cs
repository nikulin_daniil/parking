﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal Balance { get; set; }
        public  List<Vehicle> _vehicle = new List<Vehicle>(10);

        private Parking()
        {
            _vehicle.Capacity = Settings.CapacityOfParking;
        }

        public static Parking instance;

        public static Parking GetInstance()
        {
            if (instance == null) instance = new Parking();
            return instance;
        }

        public void AddVehicle(Vehicle addedVehicle)
        {
            if (addedVehicle is null) throw new ArgumentNullException();
            _vehicle.Add(addedVehicle);
        }

        public void RemoveVehicle(string Id)=>_vehicle.Remove(Find(Id));

        public void RemoveRange(ICollection<Vehicle> vehiclesToRemove)
        {
            foreach (var v in vehiclesToRemove)
             RemoveVehicle(v.Id);
            
        }

        public int AmountOfFreeSlots()
        {
         
            return _vehicle.Capacity - _vehicle.Count;
        }

        public bool Contains(Vehicle searchVehicle)
        {
            if (searchVehicle is null) return false;
            return _vehicle.Contains(searchVehicle);
        }

        public bool Contains(String Id) => Find(Id) != null;

        public Vehicle Find(string ID)
        {
            foreach (var vehicle in _vehicle)
                if (vehicle.Id == ID)
                    return vehicle;
            return null;
        }
    }
}
