﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections;
using System.Collections.Generic;

namespace CoolParking.BL.Models
 {
     public static class Settings
     {
        public static int CapacityOfParking { get; set; }
        public static decimal StartBalance { get; set; }
        public static int PeriodOfPayment { get; set; }
        public static int PeriodOfPaymentLog { get; set; }

        public static readonly Dictionary<VehicleType, decimal> Tariffs = new Dictionary<VehicleType, decimal>()
        {
            {VehicleType.PassengerCar, 20},
            {VehicleType.Truck,5}, 
            {VehicleType.Bus,3.5m}, 
            {VehicleType.Motorcycle,1}
        };

        public static  decimal CoefForFine { get; set; }

        static Settings()
        {
            CapacityOfParking = 10;
            StartBalance = 0;
            PeriodOfPayment = 5000;
            PeriodOfPaymentLog = 10000;
            CoefForFine =2.5m;
        }
     }
 }
