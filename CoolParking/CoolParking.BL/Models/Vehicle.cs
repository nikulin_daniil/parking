﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            var rx = new Regex(@"^[A-Z]{2}\-\d{4}\-[A-Z]{2}$");
            if (!rx.IsMatch(id) || balance < 0) throw new ArgumentException();
            VehicleType = vehicleType;
            Id = id;
            Balance = balance;
        }

        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; set; }

        public override string ToString()
        {
            return   Id.ToString()+"\t" + VehicleType + "\t" + Balance.ToString(CultureInfo.InvariantCulture);
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            var random = new Random();
            string dictionaryChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string dictionaryDigits= "0123456789";
            StringBuilder resultStringBuilder = new StringBuilder();
            for (int i = 0; i < 10; i++)
            {
                if (i==2||i==7)
                {
                    resultStringBuilder.Append("-");
                }
                else if (i<2||i>7)
                {
                    resultStringBuilder.Append(dictionaryChar[random.Next(dictionaryChar.Length)]);
                }
                else
                {
                    resultStringBuilder.Append(dictionaryDigits[random.Next(dictionaryDigits.Length)]);
                }
            }

            return resultStringBuilder.ToString();
        }
    }
    
}