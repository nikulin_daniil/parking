﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using System;
using System.Globalization;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string ID { get; set; }
        public decimal Sum { get; set; }
        public DateTime Time { get; set; }
        public TransactionInfo(string id, decimal sum, DateTime time)
        {
            ID = id;
            Sum = sum;
            Time = time;
        }

        public override string ToString()
        {
            return Time.ToString(CultureInfo.InvariantCulture) + "\t" + ID.ToString()+ "\t"+ Sum.ToString(CultureInfo.InvariantCulture);
        }
    }
}